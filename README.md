# Shop App Backend
1. PostgresSQL as database
2. FastAPI as Backend
3. Flutter as Frontend

## Run
`uvicorn products.main:app --reload --host 0.0.0.0`
