import yaml
import logging
import os

this_dir, this_filename = os.path.split(__file__)
CONFIG_PATH = os.path.join(this_dir, "config.yaml")

config = dict()

def init_config():
    global config
    with open(CONFIG_PATH, 'r') as file:
        config = yaml.load(file, Loader = yaml.FullLoader)
    logging.info('Config Initialized')

def get_config():
    global config
    if config == dict():
        init_config()
    return config