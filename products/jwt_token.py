from datetime import datetime, timedelta
from jose import JWTError, jwt
from typing import List, Optional
from . import schemas
from tzlocal import get_localzone

# to get a string like this run:
# openssl rand -hex 32
SECRET_KEY = "09d25e094faa6ca2556c818166b7a9563b93f7099f6f0f4caa6cf63b88e8d3e7"
ALGORITHM = "HS256"
ACCESS_TOKEN_EXPIRE_MINUTES = 30


def create_access_token(data: dict, expires_delta: Optional[timedelta] = None):
    to_encode = data.copy()
    expire = None
    if expires_delta:
        # expire = datetime.utcnow() + expires_delta
        expire = datetime.now().astimezone(get_localzone()) + expires_delta
    else:
        # expire = datetime.utcnow() + timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
        expire = datetime.now().astimezone(get_localzone()) + timedelta(
            minutes=ACCESS_TOKEN_EXPIRE_MINUTES
        )
    to_encode.update({"exp": expire})
    encoded_jwt = jwt.encode(to_encode, SECRET_KEY, algorithm=ALGORITHM)
    return encoded_jwt, expire


def verify_token(token: str, credentials_exception):
    try:
        payload = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])
        username: str = payload.get("sub")
        user_id: int = payload.get("id")
        if username is None:
            raise credentials_exception
        token_data = schemas.TokenData(username=username, user_id=user_id)
        return token_data
    except JWTError:
        raise credentials_exception
