import products
from sqlalchemy import Column, Integer, String, Float, DateTime
from sqlalchemy.sql.schema import ForeignKey, PrimaryKeyConstraint
from sqlalchemy.sql.sqltypes import Boolean
from sqlalchemy.orm import relationship
from .database import Base

# class Blog(Base):
#     __tablename__ = 'blogs'

#     id = Column(Integer, primary_key=True, index=True, autoincrement=True)
#     title = Column(String)
#     body = Column(String)


class Users(Base):
    __tablename__ = "users"

    id = Column(Integer, primary_key=True, index=True, autoincrement=True)
    name = Column(String)
    email = Column(String, unique=True)
    password = Column(String)

    # Keys
    products = relationship("Product", back_populates="products_creator")
    orders = relationship("Orders", back_populates="orders_creator")


class Product(Base):
    __tablename__ = "products"

    id = Column(Integer, primary_key=True, index=True, autoincrement=True)
    title = Column(String)
    description = Column(String)
    price = Column(Float)
    image_url = Column(String)
    is_favorite = Column(Boolean)
    is_active = Column(Boolean, default=True)
    create_by = Column(Integer, ForeignKey("users.id"))
    products_creator = relationship("Users", back_populates="products")


class ProductFavorite(Base):
    __tablename__ = "products_favorite"

    product_id = Column(Integer, ForeignKey("products.id"), nullable=False)
    user_id = Column(Integer, ForeignKey("users.id"), nullable=False)

    product = relationship("Product")
    user = relationship("Users")

    __table_args__ = (
        PrimaryKeyConstraint(product_id, user_id),
        {},
    )


class Orders(Base):
    __tablename__ = "orders"

    id = Column(Integer, primary_key=True, index=True, autoincrement=True)
    amount = Column(Float)
    date_time = Column(DateTime)
    # create_by = Column(String)

    order_details = relationship(
        "OrdersDetail", back_populates="order"
    )  # , order_by = "Orders.id")
    create_by = Column(Integer, ForeignKey("users.id"))
    orders_creator = relationship("Users", back_populates="orders")


class OrdersDetail(Base):
    __tablename__ = "orders_detail"

    order_id = Column(Integer, ForeignKey("orders.id"))
    item_id = Column(String)
    title = Column(String)
    quantity = Column(Integer)
    price = Column(Float)

    order = relationship("Orders", back_populates="order_details")

    __table_args__ = (
        PrimaryKeyConstraint(order_id, item_id),
        {},
    )
