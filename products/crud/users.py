from fastapi.exceptions import HTTPException
from .. import schemas, models
from ..database import engine, SessionLocal
from sqlalchemy.orm import Session
from sqlalchemy import exc

import logging

logger = logging.getLogger("uvicorn.error")


def update_user(db: Session, user_id: int, request: schemas.UpdateUserParams):
    user = db.query(models.Users).filter(models.Users.id == user_id)
    if not user:
        return -1, None
    user.update(vars(request), synchronize_session=False)
    db.commit()
    return user_id, user.first()


def register(db: Session, register_params: schemas.RegisterParams):
    # TODO: Add validation
    new_user = models.Users(
        name=register_params.name,
        email=register_params.email,
        password=register_params.password,
    )
    err = db.add(new_user)
    if err != None:
        logger.error(err)
        return -1

    try:
        db.commit()
        db.refresh(new_user)
    except exc.IntegrityError:
        return -1
    return new_user


def get_user_by_id(db: Session, id: int):
    user = db.query(models.Users).filter(models.Users.id == id).first()
    if not user:
        return -1, None
    return id, user


def get_user_by_username(db: Session, name: str):
    user = db.query(models.Users).filter(models.Users.email == name).first()
    if not user:
        return -1, None
    return id, user
