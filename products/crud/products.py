from itertools import product
from sqlalchemy.sql.expression import false
from .. import schemas, models
from ..database import engine, SessionLocal
from sqlalchemy.orm import Session

import logging


def create_product(db: Session, create_prod_params: schemas.CreateProduct):
    # TODO: Add validation
    new_prod = models.Product(
        title=create_prod_params.title,
        description=create_prod_params.description,
        price=create_prod_params.price,
        image_url=create_prod_params.image_url,
        is_favorite=False,
        create_by=create_prod_params.create_by,
    )
    db.add(new_prod)
    db.commit()
    db.refresh(new_prod)
    return new_prod


def get_product_by_id(db: Session, id: int):
    product = db.query(models.Product).filter(models.Product.id == id).first()
    if not product:
        return -1
    return product


def get_products_by_user_id(db: Session, id: int):
    products = db.query(models.Product).filter(models.Product.create_by == id).all()
    if not product:
        return -1
    return products


def update_favorite(db: Session, params: schemas.UpdateProductFavoriteParams):
    # product = (
    #     db.query(models.Product)
    #     .filter(models.Product.id == prod_id)
    #     .update({models.Product.is_favorite: is_favorite}, synchronize_session=False)
    # )
    # if not product:
    #     return -1
    # db.commit()

    product_favorite = db.query(models.ProductFavorite).filter(
        models.ProductFavorite.user_id == params.user_id,
        models.ProductFavorite.product_id == params.product_id,
    )
    if not product_favorite.first():
        new_fav = models.ProductFavorite(
            user_id=params.user_id, product_id=params.product_id
        )
        db.add(new_fav)
    else:
        product_favorite.delete(synchronize_session=False)
    db.commit()
    return 0


def update_product(db: Session, prod_id, update_prod_params: schemas.Product):
    product = (
        db.query(models.Product)
        .filter(models.Product.id == prod_id)
        .update(
            {
                models.Product.title: update_prod_params.title,
                models.Product.description: update_prod_params.description,
                models.Product.price: update_prod_params.price,
                models.Product.image_url: update_prod_params.image_url,
            },
            synchronize_session=False,
        )
    )
    if not product:
        return -1
    db.commit()
    return 0


def get_favorites_by_user_id(db: Session, id: int):
    product = (
        db.query(models.ProductFavorite)
        .filter(models.ProductFavorite.user_id == id)
        .all()
    )
    return product


def get_all_products(db: Session):
    products = db.query(models.Product).order_by(models.Product.id).all()
    return products


def remove_product_by_id(db: Session, id):
    prod = (
        db.query(models.Product)
        .filter(models.Product.id == id)
        .delete(synchronize_session=False)
    )
    if not prod:
        return -1
    db.commit()
    return 0
