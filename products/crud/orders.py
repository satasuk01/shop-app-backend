from .. import schemas, models
from sqlalchemy.orm import Session

import logging

logger = logging.getLogger("uvicorn.error")


def add_order(
    db: Session, create_order_params: schemas.CreateOrderParams, create_by: str
):
    order = models.Orders(
        amount=create_order_params.amount,
        date_time=create_order_params.date_time,
        create_by=create_by,
    )
    db.add(order)
    db.flush()

    for orderDetail in create_order_params.products:
        od = models.OrdersDetail(
            order_id=order.id,
            item_id=orderDetail.id,
            title=orderDetail.title,
            quantity=orderDetail.quantity,
            price=orderDetail.price,
        )
        db.add(od)

    db.commit()
    db.refresh(order)
    return order


def get_orders_by_user_id(db: Session, id: int):
    orders = db.query(models.Orders).filter(models.Orders.create_by == id).all()
    return orders


def get_all_order(db: Session):
    orders = db.query(models.Orders).order_by(models.Orders.id).all()
    return orders


def get_orders_detail_by_order_id(db: Session, id: int):
    orders_detail = (
        db.query(models.OrdersDetail).filter(models.OrdersDetail.order_id == id).all()
    )
    # logger.fatal(orders_detail[0])
    return orders_detail
