import products
from products.database import Base
from pydantic import BaseModel
from typing import Any, List
from sqlalchemy.sql.expression import true
from datetime import datetime


class RegisterParams(BaseModel):
    class Config():
        orm_mode = true
    name: str
    email: str
    password: str

class LoginParams(BaseModel):
    class Config():
        orm_mode = true
    email: str
    password: str

    