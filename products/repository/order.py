from sqlalchemy.orm import Session
from sqlalchemy.sql.functions import user
from .. import schemas
from ..crud import orders as order_crud

import logging

logger = logging.getLogger("uvicorn.error")


def get_all_orders(db: Session):
    orders = order_crud.get_all_order(db)
    return orders


def get_orders_by_user_id(user_id: int, db: Session):
    orders = order_crud.get_orders_by_user_id(db, user_id)
    return orders


def create_order(request: schemas.CreateOrderParams, current_user: str, db: Session):
    new_order = order_crud.add_order(db, request, current_user)
    return new_order
