from fastapi import HTTPException, status
from sqlalchemy.orm import Session
from .. import models, schemas
from ..crud import products as prod_crud

import logging

logger = logging.getLogger("uvicorn.error")


def get_all_products(db: Session):
    all_products = prod_crud.get_all_products(db)
    return all_products


def get_product_by_id(id: int, db: Session):
    prod = prod_crud.get_product_by_id(db, id)
    if prod == -1:
        raise HTTPException(status.HTTP_404_NOT_FOUND, f"Product {id} is not found")
    return prod


def get_products_by_user_id(id: int, db: Session):
    prod = prod_crud.get_products_by_user_id(db, id)
    if prod == -1:
        raise HTTPException(status.HTTP_404_NOT_FOUND, f"Product {id} is not found")
    return prod


def get_my_favorites(user_id: int, db: Session):
    products = prod_crud.get_favorites_by_user_id(db, user_id)
    res = schemas.ProductFavoriteResponse(favorites=[])
    for product in products:
        res.favorites.append(product.product_id)
    return res


def delete_product(id: int, db: Session):
    prod_id = prod_crud.remove_product_by_id(db, id)
    if prod_id != 0:
        raise HTTPException(status.HTTP_404_NOT_FOUND, f"Product {id} is not found")
    return {"detail": f"delete {id}"}


def add_product(request: schemas.CreateProduct, db: Session):
    new_prod = prod_crud.create_product(db, request)
    return new_prod


def update_product(prod_id: int, request: schemas.UpdateProduct, db: Session):
    try:
        int(prod_id)
    except:
        raise HTTPException(
            status.HTTP_404_NOT_FOUND, f"Product {prod_id} is not found"
        )
    update_res = prod_crud.update_product(db, prod_id, request)
    if update_res != 0:
        raise HTTPException(
            status.HTTP_404_NOT_FOUND, f"Product {prod_id} is not found"
        )
    db.commit()
    return {"detail": f"update {prod_id}"}


def update_favorite(
    prod_id, favorite_params: schemas.UpdateProductFavoriteParams, db: Session
):
    update_res = prod_crud.update_favorite(db, favorite_params)
    if update_res != 0:
        raise HTTPException(
            status.HTTP_404_NOT_FOUND, f"Product {prod_id} is not found"
        )
    db.commit()
    return {"detail": f"update favorite {prod_id}"}
