from fastapi import HTTPException, status
from sqlalchemy.orm import Session
from .. import models, schemas
from ..crud import users as user_crud
from ..utils.hashing import Hash

import logging

logger = logging.getLogger("uvicorn.error")


def create_user(request: schemas.RegisterParams, db: Session):
    # logger.info('/register '+ str(request))
    hashedPassword = Hash.bcrypt(request.password)
    request.password = hashedPassword
    new_user = user_crud.register(db, request)
    if new_user == -1:
        raise HTTPException(status.HTTP_409_CONFLICT, f"{request.email} is exist")
    return new_user


def get_user_by_id(id: int, db: Session):
    result, user = user_crud.get_user_by_id(db, id)
    if result == -1:
        raise HTTPException(status.HTTP_404_NOT_FOUND, f"User {id} is not found")
    return user


def get_user_by_username(name: str, db: Session):
    result, user = user_crud.get_user_by_username(db, name)
    if result == -1:
        raise HTTPException(status.HTTP_404_NOT_FOUND, f"User {name} is not found")
    return user


def update_user(request: schemas.UpdateUserParams, user_id: int, db: Session):
    result, user = user_crud.update_user(db, user_id, request)
    if result == -1:
        raise HTTPException(status.HTTP_404_NOT_FOUND, f"User {user_id} is not found")
    return user
