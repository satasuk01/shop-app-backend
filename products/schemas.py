from itertools import product
from sqlalchemy.sql.sqltypes import Boolean
import products
from products.database import Base
from pydantic import BaseModel
from typing import Any, List, Optional
from sqlalchemy.sql.expression import true
from datetime import datetime

# User
class RegisterParams(BaseModel):
    class Config:
        orm_mode = true

    name: str
    email: str
    password: str


class ShowUser(BaseModel):
    class Config:
        orm_mode = true

    id: int
    name: str
    email: str
    # username: str = ''


class LoginParams(BaseModel):
    class Config:
        orm_mode = true

    email: str
    password: str


# Products
class Product(BaseModel):
    class Config:
        orm_mode = true

    id: int
    title: str
    description: str
    price: float
    image_url: str
    is_favorite: bool
    create_by: Optional[int]
    creator: Optional[str]


class CreateProduct(BaseModel):
    title: str
    description: str
    price: float
    image_url: str
    # is_favorite: bool
    create_by: Optional[int]


class UpdateProduct(Product):
    id: Any
    is_favorite: Optional[bool]


class UpdateProductFavoriteParams(BaseModel):
    class Config:
        orm_mode = true

    is_favorite: bool
    user_id: Optional[int]
    product_id: Optional[int]


class ProductFavoriteResponse(BaseModel):
    class Config:
        orm_mode = true

    favorites: List[int]


# Orders
class OrderDetail(BaseModel):
    class Config:
        orm_mode = true

    id: str
    title: str
    quantity: int
    price: float
    # create_by: ShowUser


class CreateOrderParams(BaseModel):
    class Config:
        orm_mode = true

    amount: float
    date_time: datetime
    products: List[OrderDetail]


class Order(BaseModel):
    class Config:
        orm_mode = true

    id: str
    amount: float
    date_time: datetime
    # TODO: Make it better
    # order_details: List[OrderDetail] = []
    order_details: list
    # create_by: ShowUser


# Authentication
class UpdateUserParams(BaseModel):
    class Config:
        orm_mode = true

    name: str


class Login(BaseModel):
    username: str
    password: str


class Token(BaseModel):
    access_token: str
    token_type: str


class TokenData(BaseModel):
    username: Optional[str] = None
    user_id: Optional[int] = None
