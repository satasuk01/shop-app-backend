from typing import List
from fastapi import Depends, status, APIRouter
from starlette.status import HTTP_200_OK
from .. import schemas, oauth2
from ..database import get_db
from sqlalchemy.orm import Session
from ..repository import product

import logging

logger = logging.getLogger("uvicorn.error")

router = APIRouter(
    prefix="/products", tags=["products"], responses={404: {"message": "Not found"}}
)


@router.get(
    "/all",
    response_model=List[schemas.Product],
    status_code=status.HTTP_200_OK,
    tags=["products"],
)
async def get_all_products(db: Session = Depends(get_db)):
    return product.get_all_products(db)


@router.get(
    "/my-favorites",
    response_model=schemas.ProductFavoriteResponse,
    status_code=status.HTTP_200_OK,
)
def get_my_favorites(
    db: Session = Depends(get_db),
    current_user: schemas.TokenData = Depends(oauth2.get_current_user),
):
    return product.get_my_favorites(current_user.user_id, db)


@router.get("/product/{id}", response_model=schemas.Product)
def get_product_by_id(id: int, db: Session = Depends(get_db)):
    return product.get_product_by_id(id, db)


@router.get("/user/{id}", response_model=List[schemas.Product])
def get_products_by_user_id(id: int, db: Session = Depends(get_db)):
    return product.get_products_by_user_id(id, db)


@router.get("/my-products", response_model=List[schemas.Product])
def get_products_by_user_id(
    db: Session = Depends(get_db),
    current_user: schemas.TokenData = Depends(oauth2.get_current_user),
):
    return product.get_products_by_user_id(current_user.user_id, db)


@router.post("/add")
def add(
    request: schemas.CreateProduct,
    db: Session = Depends(get_db),
    current_user: schemas.TokenData = Depends(oauth2.get_current_user),
):
    request.create_by = current_user.user_id
    return product.add_product(request, db)


@router.put("/update/{prod_id}", status_code=status.HTTP_202_ACCEPTED)
def update_product(
    prod_id, request: schemas.UpdateProduct, db: Session = Depends(get_db)
):
    return product.update_product(prod_id, request, db)


@router.put("/update-favorite/{prod_id}", status_code=status.HTTP_202_ACCEPTED)
def update_favorite(
    prod_id,
    favorite_params: schemas.UpdateProductFavoriteParams,
    db: Session = Depends(get_db),
    current_user: schemas.TokenData = Depends(oauth2.get_current_user),
):
    favorite_params.user_id = current_user.user_id
    favorite_params.product_id = prod_id
    return product.update_favorite(prod_id, favorite_params, db)


@router.delete("/remove/{id}", status_code=status.HTTP_204_NO_CONTENT)
def delete_product(id, db: Session = Depends(get_db)):
    return product.delete_product(id, db)
