from products.models import Users
from fastapi import FastAPI, Depends, status, Response, HTTPException, APIRouter
import logging

from products.crud import users as users_crud
from .. import schemas, oauth2
from ..database import get_db
from sqlalchemy.orm import Session
from ..repository import user

logger = logging.getLogger("uvicorn.error")

router = APIRouter(
    prefix="/users", tags=["Users"], responses={404: {"message": "Not found"}}
)


@router.post("/", response_model=schemas.ShowUser)
def register(request: schemas.RegisterParams, db: Session = Depends(get_db)):
    return user.create_user(request, db)


@router.get("/me", response_model=schemas.ShowUser)
def get_active_user(
    current_user: schemas.ShowUser = Depends(oauth2.get_current_user),
    db: Session = Depends(get_db),
):
    logger.info(current_user.username)
    return user.get_user_by_username(current_user.username, db)


@router.get("/id/{id}", response_model=schemas.ShowUser)
def get_user_by_id(id: int, db: Session = Depends(get_db)):
    return user.get_user_by_id(id, db)


@router.get("/name/{username}", response_model=schemas.ShowUser)
def get_user_by_username(username: str, db: Session = Depends(get_db)):
    return user.get_user_by_username(username, db)


@router.put("/update", response_model=schemas.ShowUser)
def update_user(
    request: schemas.UpdateUserParams,
    current_user: schemas.ShowUser = Depends(oauth2.get_current_user),
    db: Session = Depends(get_db),
):
    return user.update_user(request, current_user.user_id, db)
