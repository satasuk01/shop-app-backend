from fastapi import APIRouter, Depends, HTTPException, status
from fastapi.param_functions import Depends
from fastapi.security.oauth2 import OAuth2PasswordRequestForm
from sqlalchemy.orm.session import Session
from datetime import timedelta
from .. import schemas, database, models, oauth2
from ..utils.hashing import Hash
from .. import jwt_token as token

router = APIRouter(tags=["Authentication"], responses={404: {"message": "Not found"}})


@router.post("/login")
def login(
    request: OAuth2PasswordRequestForm = Depends(),
    db: Session = Depends(database.get_db),
):
    user = db.query(models.Users).filter(models.Users.email == request.username).first()
    if not user:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail="Invalid Credentials"
        )

    if not Hash.verify(user.password, request.password):
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail="Invalid Credentials"
        )

    # Generate JWT Token and return
    ACCESS_TOKEN_EXPIRE_MINUTES = 300
    access_token_expires = timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
    access_token, expire = token.create_access_token(
        data={"sub": user.email, "id": user.id},  # , "scopes": form_data.scopes},
        expires_delta=access_token_expires,
    )

    return {
        "access_token": access_token,
        "token_type": "bearer",
        "errors": None,
        "expire": expire,
        "user_id": user.id,
        "username": user.email,
    }
