from typing import List
from fastapi import FastAPI, Depends, status, Response, HTTPException, APIRouter
from .. import schemas, models, oauth2
from ..database import get_db
from ..repository import order
from sqlalchemy.orm import Session

import logging

logger = logging.getLogger("uvicorn.error")

router = APIRouter(
    prefix="/orders", tags=["orders"], responses={404: {"message": "Not found"}}
)


@router.get("/all", response_model=List[schemas.Order], status_code=status.HTTP_200_OK)
def get_all_order(db: Session = Depends(get_db)):
    return order.get_all_orders(db)


@router.get(
    "/my-orders", response_model=List[schemas.Order], status_code=status.HTTP_200_OK
)
def get_my_orders(
    db: Session = Depends(get_db),
    current_user: schemas.TokenData = Depends(oauth2.get_current_user),
):
    return order.get_orders_by_user_id(current_user.user_id, db)


@router.post("/create")
# def create_order(request: schemas.CreateOrderParams, db: Session = Depends(get_db)):
def create_order(
    request: schemas.CreateOrderParams,
    db: Session = Depends(get_db),
    current_user: schemas.TokenData = Depends(oauth2.get_current_user),
):
    return order.create_order(request, current_user.user_id, db)
