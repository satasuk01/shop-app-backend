import logging
from sqlalchemy import create_engine
from sqlalchemy.sql.expression import false
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

from .config import get_config

config_db = get_config()['database']
logging.warning(config_db)

# SQLALCHEMY_DB_URL = 'sqlite:///./blog.db'
# SQLALCHEMY_DATABASE_URL = "postgresql://postgres:postgres@localhost/shop_app"
SQLALCHEMY_DATABASE_URL = f"{config_db['driver']}://{config_db['user']}:{config_db['password']}@{config_db['host']}/{config_db['databaseName']}"

engine = create_engine(SQLALCHEMY_DATABASE_URL, echo = config_db['echo'])

SessionLocal = sessionmaker(bind=engine, autocommit=False, autoflush=False)

Base = declarative_base()

def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()