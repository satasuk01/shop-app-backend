from typing import Optional

from fastapi import FastAPI
from pydantic import BaseModel
import uvicorn

from libs import add
print(add(2,3))

app = FastAPI()

@app.get('/')
def welcome():
    return {'data':{'message':"Hello"}}

@app.get('/blog')
def index(limit: int=10, published: bool = True, sort: Optional[str] = None):
    if published:
        return{'data':f'blog list of {limit}'}
    else:
        return{'data':f'unplubished x {limit}'}

@app.get('/blog/unpublish')
def unpublish():
    '''Show All Unpublished Blogs'''
    return{'data':'Non Publish'}

@app.get('/blog/{id}')
def show(id: int):
    return{'data':id}

@app.get('/blog/{id}/comments')
def show_comment(id: int, limit=10):
    return{'data':'comment {}'.format(id)}

@app.get('/about')
def about():
    return {'data':{'about':"Satasuk"}}


# === Post Method ===
class Blog(BaseModel):
    title: str
    body: str
    published: Optional[bool] = None

list_of_blogs = []

@app.post('/blog')
def create_blog(blog: Blog):
    list_of_blogs.append(blog)
    return {'data': 'blog is created', 'blogs': list_of_blogs}

# if __name__ == "__main__":
#     uvicorn.run(app, host="127.0.0.1", port=8000)


# @app.get("/")
# def read_root():
#     return {"Hello": "World"}


# @app.get("/items/{item_id}")
# def read_item(item_id: int, q: Optional[str] = None):
#     return {"item_id": item_id, "q": q}


# @app.put("/items/{item_id}")
# def update_item(item_id: int, item: Item):
#     return {"item_name": item.name, "item_id": item_id}