from sqlalchemy import create_engine
from sqlalchemy.sql.expression import false
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

# SQLALCHEMY_DB_URL = 'sqlite:///./blog.db'
SQLALCHEMY_DATABASE_URL = "postgresql://postgres:postgres@localhost/app"

engine = create_engine(SQLALCHEMY_DATABASE_URL, echo = True)

SessionLocal = sessionmaker(bind=engine, autocommit=False, autoflush=False)

Base = declarative_base()