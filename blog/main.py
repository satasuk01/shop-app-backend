from typing import List
from fastapi import FastAPI, Depends, status, Response, HTTPException
from . import schemas, models
from .database import engine, SessionLocal
from sqlalchemy.orm import Session

app = FastAPI()

models.Base.metadata.create_all(engine)

def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()

@app.post('/blog' ,tags=["blogs"])
def create(request: schemas.Blog, db: Session = Depends(get_db)):
    new_blog = models.Blog(title=request.title, body = request.body)
    db.add(new_blog)
    db.commit()
    db.refresh(new_blog)
    return new_blog

@app.get('/blog',response_model=List[schemas.ShowBlog], status_code=status.HTTP_201_CREATED, tags=["blogs"])
def get_all_blogs(db: Session = Depends(get_db)):
    blogs = db.query(models.Blog).all()
    return blogs

@app.get('/blog/{id}', status_code=status.HTTP_200_OK, response_model=schemas.ShowBlog , tags=["blogs"])
def get_blog(id, response: Response, db: Session = Depends(get_db)):
    blog = db.query(models.Blog).filter(models.Blog.id == id).first()
    if not blog:
        # Use with response: Response
        # response.status_code = status.HTTP_404_NOT_FOUND
        # return {'detail': f'Blog {id} is not found'}
        raise HTTPException(status.HTTP_404_NOT_FOUND, f'Blog {id} is not found')
    return blog

@app.delete('/blog/{id}', status_code=status.HTTP_204_NO_CONTENT, tags=["blogs"])
def delete_blog(id, db: Session = Depends(get_db)):
    # Query blog
    blog = db.query(models.Blog).filter(models.Blog.id == id).delete(synchronize_session=False)
    if not blog:
        raise HTTPException(status.HTTP_404_NOT_FOUND, f'Blog {id} is not found')
    db.commit()
    return {'detail': f'delete {id}'}

@app.put('/blog/{id}', status_code=status.HTTP_202_ACCEPTED, tags=["blogs"])
def update_blog(id,request: schemas.Blog, db: Session = Depends(get_db)):
    blog = db.query(models.Blog).filter(models.Blog.id == id).update({
        'title': request.title,
        'body': request.body,
        },synchronize_session=False)
    if not blog:
        raise HTTPException(status.HTTP_404_NOT_FOUND, f'Blog {id} is not found')
    db.commit()
    return {'detail': f'update {id}'}


@app.post('/user', tags=["users"])
def create_user(request: schemas.User, db: Session = Depends(get_db)):
    new_user = models.User(name = request.name, email = request.email, password = request.password)
    db.add(new_user)
    db.commit()
    db.refresh(new_user)
    return new_user