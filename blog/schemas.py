from pydantic import BaseModel
from sqlalchemy.sql.expression import true

class Blog(BaseModel):
    title: str
    body: str

class ShowBlog(Blog):
    class Config():
        orm_mode = true


class User(BaseModel):
    name: str
    email: str
    password: str